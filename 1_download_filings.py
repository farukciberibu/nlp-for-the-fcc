# Download all filings from csv
import csv
import re
import requests

i = 0

with open('ecfsresults.csv', newline='') as csvfile:
    spamreader = csv.reader(csvfile, delimiter=',', quotechar='"')
    for row in spamreader:
        if i == 0:
            i = i + 1
            continue

        if row:
            url = re.findall(r'"([^"]*)"', row[0])[0]

            r = requests.get(url, stream=True)

            if str(url).endswith("pdf"):
                with open('filings/' + str(i) + '.pdf', 'wb') as f:
                    print("Downloading " + str(i) + url[-4:])
                    f.write(r.content)
                    f.close()
            elif str(url).endswith("docx"):
                with open('filings/' + str(i) + '.docx', 'wb') as f:
                    print("Downloading " + str(i) + url[-4:])
                    f.write(r.content)
                    f.close()
            elif str(url).endswith("txt"):
                with open('filings/' + str(i) + '.txt', 'wb') as f:
                    print("Downloading " + str(i) + url[-4:])
                    f.write(r.content)
                    f.close()
        i = i + 1
