import re

from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
from nltk.stem import PorterStemmer
from nltk.stem import WordNetLemmatizer

review = "<b>sadasda!!</b>. There is an in-built stopword list in NLTK which we can use to remove stop words from text documents. However this is not the standard stopwords list for every problem, we can also define our own set of stop words based on the domain."

cleaned_review = re.sub(re.compile("<.*?>"), '', review)

cleaned_review = re.sub("[^A-Za-z0-9]+", ' ', cleaned_review)

cleaned_review = cleaned_review.lower()

tokens = word_tokenize(cleaned_review)

print(cleaned_review)
print(tokens)

stop_words = stopwords.words('english')
filtered_review = [word for word in tokens if word not in stop_words]
print(filtered_review)

stemmer = PorterStemmer()
stemmed_review = [stemmer.stem(word) for word in filtered_review]
print(stemmed_review)

lemmatizer = WordNetLemmatizer()
lemm_review = [lemmatizer.lemmatize(word) for word in filtered_review]
print(lemm_review)
