# Collocation detection (Los Angeles, Moby Dick, New York, etc.)
from nltk.collocations import BigramCollocationFinder, BigramAssocMeasures

from nltk.corpus import stopwords
from nltk.book import text2

stopwords_ = set(stopwords.words('english'))

words = [word.lower() for document in text2
         for word in document.split()
         if len(word) > 2 and word not in stopwords_]

finder = BigramCollocationFinder.from_words(words)
bgm = BigramAssocMeasures()
collocations = {bigram: pmi for bigram, pmi in finder.score_ngrams(bgm.mi_like)}
print(collocations)
