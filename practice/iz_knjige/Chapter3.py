import re

pattern = re.compile("at")

re.search(pattern, 'later')
re.match(pattern, 'later')

pattern1 = re.compile("fr?og")
pattern2 = re.compile("hello+")
pattern3 = re.compile("cooo*l")

pattern = re.compile('[bcr]at')
document = 'the batter won the game'
matches = re.match(pattern, document)  # returns None
searches = re.search(pattern, document)  # matches 'bat'
