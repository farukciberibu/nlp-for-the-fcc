# Count vectorizer (aka bag of words)
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer
from nltk.book import text2

vectorizer = CountVectorizer(analyzer='word')

X = vectorizer.fit_transform(text2)

# Extract all n-gram counts with a specified range, minimum to be in 10 documents and not in more than 75% of all
# documents
vectorizer = CountVectorizer(analyzer='word',
                             ngram_range=(1, 3),
                             min_df=10,
                             max_df=0.75,
                             stop_words='english')

X = vectorizer.fit_transform(text2)

# Get words that are moderately frequent but occur in only a few documents (weighted)
tfidf_vectorizer = TfidfVectorizer(analyzer='word',
                                   min_df=0.001,
                                   max_df=0.75,
                                   stop_words='english',
                                   sublinear_tf=True)

X = tfidf_vectorizer.fit_transform(text2)

# Implementation of nearest neighbors in vector space (ex. apartment, flat)
import numpy as np


def nearest_neighbors(value, vectors, n=10):
    # compute cosine similarities
    ranks = np.dot(value, vectors.T) / np.sqrt(np.sum(vectors ** 2, 1))
    # sort by similarity, reverse order, and get the top N
    neighbors = [idx for idx in ranks.argsort()[::-1]][:n]
    return neighbors


# Word2Vec implementation
from gensim.models import Word2Vec
from gensim.models.word2vec import FAST_VERSION

# initialize model
w2v_model = Word2Vec(size=300,
                     window=5,
                     hs=0,
                     sample=0.000001,
                     negative=5,
                     min_count=10,
                     workers=-1,
                     iter=50)

w2v_model.build_vocab(text2)

w2v_model.train(text2, total_examples=w2v_model.corpus_count, epochs=w2v_model.epochs)

# Get vector of word from vec2word and compare and get similar words
word1 = "love"
word2 = "heart"

# retrive the actual vector
w2v_model.wv[word1]

# compare
w2v_model.wv.similarity(word1, word2)

# get the 3 most similar words
w2v_model.wv.most_similar(word1, topn=3)

# Get analogous word (king+woman-man = queen)
w2v_model.wv.most_similar(positive=['king', 'woman'], negative=['man'])

# Doc2Vec
from gensim.models import Doc2Vec
from gensim.models.doc2vec import TaggedDocument

corpus = []
for docid, document in enumerate(text2):
    corpus.append(TaggedDocument(document.split(), tags=["{0:0>4}".format(docid)]))

d2v_model = Doc2Vec(size=300,
                    window=5,
                    hs=0,
                    negative=5,
                    workers=-1,
                    iter=50,
                    dm=0,
                    dbow_words=1)

d2v_model.build_vocab(corpus)

d2v_model.train(corpus, total_examples=d2v_model.corpus_count, epochs=d2v_model.epochs)

target_doc = '0002'

# retrieve most similar documents
d2v_model.docvecs.most_similar(target_doc, topn=5)
