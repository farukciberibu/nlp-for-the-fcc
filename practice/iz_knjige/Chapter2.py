import spacy

nlp = spacy.load('en_core_web_sm')

documents = "I've been 2 times to New York in 2011, but did not have the constitution for it. It DIDN'T appeal to me. " \
            "I preferred Los Angeles. "

# Tokenization
tokens = [[token.text for token in sentence] for sentence in nlp(documents).sents]

# Lemmatization
lemmas = [[token.lemma_ for token in sentence]
          for sentence in nlp(documents).sents]

# Stemming
from nltk import SnowballStemmer

stemmer = SnowballStemmer('english')
stems = [[stemmer.stem(token) for token in sentence] for sentence in tokens]

# N grams
from nltk import ngrams

bigrams = [gram for gram in ngrams(tokens[0], 2)]

# POS tagging
pos = [[token.pos_ for token in sentence] for sentence in nlp(documents).sents]

# Removing stop words
content = [[token.text for token in sentence if token.pos_ in {'NOUN', 'VERB', 'PROPN', 'ADJ', 'ADV'}
            and not token.is_stop] for sentence in nlp(documents).sents]

# Named entity recognition
entities = [[(entity.text, entity.label_) for entity in nlp(sentence.text).ents]
            for sentence in nlp(documents).sents]

var = [[(c.text, c.head.text, c.dep_) for c in nlp(sentence.text)] for sentence in nlp(documents).sents]
