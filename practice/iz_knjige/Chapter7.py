# Simple K-means clustering
from sklearn.cluster import KMeans

km = KMeans(n_clusters=10,
            n_jobs=-1)

clusters = km.fit_predict(X)
centroids = km.cluster_centers_


