We server just over 1000 rural customers in central Pennsylvania and we offer them up to 15mbps with traditional fixed point wireless technologies such as 5GHz Wifi based solutions.

We recently started deploying LTE in the 3.5GHz band and have invested about $70,000 in the adventure so far which will give us access to many customers that we otherwise cannot reach.

Gaining access to the CBRS band will allow us to more widely deploy our LTE solution because it will allow to more densely deploy units due to the extra spectrum availability.  This will also allow larger channel size which will help with congestion and througput issues that sometimes hurt our customers.  These changes will let us reach customers that we currently cannot get to with our exisitng LTE or WiFi based solutions, these customers currently have no options for broadband internet.

For these reasons, In The Stix Broadband, LLC oppose both the CTIA and T-Mobile petitions.