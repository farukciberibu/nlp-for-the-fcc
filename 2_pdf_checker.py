# Checks to see if scanned PDF
import fitz
import os

for filename in os.listdir("filings"):

    if filename.endswith("pdf"):
        text = ""
        doc = fitz.open("filings/" + filename)
        for page in doc:
            text += page.getText()

        if len(text) == 0:
            print(filename)
