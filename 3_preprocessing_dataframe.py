import os
from docx import Document
import pandas as pd

# Read original CSV and increment index by since filings start from 1
df = pd.read_csv("ecfsresults_original.csv")
df.index = df.index + 1
# Only use important columns
columns = ['Date Received', 'Type of Filing', 'Submission Type', 'Proceeding ID', 'Name of Filer(s)',
           'Total Page Count']
df = df[columns]

folder = 'filings/docs/formatted'

# For each filing add the content of it to the respective filing id (row)
for filename in os.listdir(folder):
    filing_id = os.path.splitext(filename)[0]

    if filename.endswith("docx"):
        document = Document(folder + '/' + filename)
        text = ''
        for para in document.paragraphs:
            text += ' ' + str(para.text)

        df.loc[df.index[int(filing_id) - 1], 'Content'] = text

    if filename.endswith("txt"):
        text = open(folder + "/" + filename).read()
        df.loc[df.index[int(filing_id) - 1], 'Content'] = text

df = df[df['Content'].notna()]
print(df)

df.to_csv(r'C:\Users\User\PycharmProjects\filings-downloader\processed_dataframe2.csv', header=True)
